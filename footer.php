            <div id="footer">
                <div class="row">
                        <div class="small-12 medium-4 columns footer-widget">
                            <?php if ( dynamic_sidebar('footer_left') ) : else : endif; ?>
                        </div>
                        <div class="small-12 medium-4 columns footer-widget">
                            <?php if ( dynamic_sidebar('footer_middle') ) : else : endif; ?>
                        </div>
                        <div class="small-12 medium-4 columns footer-widget">
                            <?php if ( dynamic_sidebar('footer_right') ) : else : endif; ?>
                        </div>
                </div>
                <div class="row">
                    <div class="small-12 medium-4 columns" style="float:left">
                        &copy; <?php echo get_bloginfo('name')." ".date('Y'); ?>. All rights reserved.
                    </div>
                    <div id="footer-menu" class="small-12 medium-8 columns">
                        <?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>  
                    </div>
                </div>
            </div>

<!-- End container, body and HTML -->
        </div>
<!-- Add Wordpress Footer and initialise Zurb/Foundation JS -->
        <?php wp_footer(); ?>
        <script>
          $(document).foundation();
        </script>
    </body>
</html>