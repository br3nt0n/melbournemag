<?php get_header(); ?>
<?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>
<div id="content" class="row">
    <div class="small-12 columns">
        <h1><?php the_title(); ?></h1>
        <hr>
        <p class="post-meta"><span class="post-exclusive">Exclusive</span>By <?php the_author_posts_link();?>, <?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></p>
        <hr>
        <div id="content" class="row">
            <div class="small-12 medium-12 columns">
            <?php if(has_post_thumbnail()) {
                the_post_thumbnail('large');
    get_post(get_post_thumbnail_id())->post_excerpt; 

            }?>
            </div>
            
        </div>
        <div class="row">
            <div class="small-12 medium-8 medium-offset-2 columns" style="float:left;">
                     <?php the_content(); ?>
                <div class="author-box">
                    <h2><?php the_author_posts_link();?></h2>
                    <?php echo get_avatar( get_the_author_meta( 'ID' ), 32 ); ?>
                    <p> <?php echo get_the_author_meta('description'); ?> </p>
                </div>
                <!-- End the Wordpress Loop -->
                <?php endwhile;
                endif; ?>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>