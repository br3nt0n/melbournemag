# MelbourneMag Theme

This is the working repository for the MelbourneMag theme.

#Features
- Posts near me (add suburb to post meta, and then use HTML5 geolocation)?
- RTL support
- TinyPass integration
- Responsive design
- Chrome app

## Requirements

You'll need to have the following items installed before continuing.

  * [Wordpress](http://wordpress.org): Use the installer provided on the Wordpress website.

## Quickstart


## Directory Structure

