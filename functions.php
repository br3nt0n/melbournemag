<?php 
//##################### MelbourneMag setup ####################
add_action( 'widgets_init', 'register_melb_sidebars' );
add_action( 'init', 'register_melb_menus' );
add_theme_support( 'post-thumbnails' ); 

//Setup widget areas
function register_melb_sidebars() {
    add_sidebar('Footer Left', 'footer_left', '<div>', '</div>', '<h2 class="rounded">', '</h2>');
    add_sidebar('Footer Middle', 'footer_middle', '<div>', '</div>', '<h2 class="rounded">', '</h2>');
    add_sidebar('Footer Right', 'footer_right', '<div>', '</div>', '<h2 class="rounded">', '</h2>');
    add_sidebar('Sidebar', 'sidebar', '<div>', '</div>', '<h2 class="rounded">', '</h2>');
}

//Register the sidebar
function add_sidebar($name, $id, $before_widget, $after_widget, $before_title, $after_title) {
    register_sidebar( array(
		'name' => $name,
		'id' => $id,
		'before_widget' => $before_widget,
		'after_widget' => $after_widget,
		'before_title' => $before_title,
		'after_title' => $after_title,
	) );
}

//Setup menus
function register_melb_menus() {
  register_nav_menus(
    array(
      'top-menu' => __( 'Top Menu' ),
      'main-menu' => __( 'Main Menu' ),
      'footer-menu' => __( 'Footer Menu' )
    )
  );
}

//Add foundation JavaScript
function enqueue_melb_scripts() {
    wp_enqueue_script('jquery-melb', get_stylesheet_directory_uri() . '/js/vendor/jquery.js', false, '1.0', true );
    wp_enqueue_script('fastclick', get_stylesheet_directory_uri() . '/js/vendor/fastclick.js', false, '1.0', true );
    wp_enqueue_script('foundation', get_stylesheet_directory_uri() . '/js/foundation.min.js', false, '5.0', true );
}

add_action( 'wp_enqueue_scripts', 'enqueue_melb_scripts' );

function first_paragraph($content){
	return preg_replace('/<p([^>]+)?>/', '<p$1 class="lead">', $content, 1);
}
add_filter('the_content', 'first_paragraph');

?>