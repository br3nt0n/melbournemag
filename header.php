<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title> <?php echo get_the_title(); ?> </title>
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" type="text/css" media="screen" />
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
        <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
        <link href='http://fonts.googleapis.com/css?family=Bitter' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
        <script src="<?php echo get_template_directory_uri(); ?>/js/vendor/modernizr.js"></script>
        <?php wp_head(); ?>
    </head>
    <body>
        
        
    <div class="container">
    <!--Top menu -->
    <div id="top-header">
        <div class="row hide-for-small-only">
            <div class="small-12 medium-8 columns" style="float:left">
            <?php wp_nav_menu( array( 'theme_location' => 'top-menu' ) ); ?> 
            </div>
            <div class="medium-4 contact-flyout columns" style="float:right; text-align:right;">
            News Tip Expander 
            </div>
        </div>
    </div>
         
    <!-- Header logo and advertisement -->
        <div id="header">
            <div class="row">
                <div class="small-12 columns">
                    <div class="medium-4 columns">
                        <span style="float:left">
                            <img width="230px" src="<?php echo get_template_directory_uri(); ?>/logo%20(1).png"/>    
                        </span>
                    </div>
                    <div class="medium-8 columns">
                        <span style="float:right">
                        <img src="<?php echo get_template_directory_uri(); ?>/leaderboard.jpg" /> 
                        </span>
                    </div>
                </div>
            </div>
        </div>
    
    <!-- Main menu -->
    <div id="main-menu">
        <div class="row">
            <div class="hide-for-small-only medium-12 columns">
            <?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>  
            </div>
        </div>
    </div>