<?php get_header(); ?>
<div id="content" class="row">
    <div class="small-12 columns">
        <div class="row">
            <div class="small-12 medium-8 columns" style="float:left;">
                <div class="row featured-posts">
                    <!-- Home page featured posts wall -->
                    <div class="small-12 medium-6 columns">
                        <!-- Left, large featured image -->
                        <?php $query_featured = new WP_Query( array ('tag' => 'premium', 'posts_per_page' => 4) ); 
                       while ( $query_featured->have_posts() ) {
	$query_featured->the_post();?>
	<li> <?php the_title() ?></li>
<?php }     
                        wp_reset_postdata();
                        ?>
                    </div>
                    <div class="small-12 medium-3 columns">
                        <!-- Centre, smaller featured image -->
                        2 x smaller images on top of one another
                    </div>
                    <div class="small-12 medium-3 columns">
                        <!-- Left, large featured image -->
                         2 x smaller images on top of one another
                    </div>
                </div>
                <!-- Insert tabs for sorting by latest, or popularity -->
                <dl class="tabs" data-tab>
                  <dd class="active"><a href="#panel2-1">Latest</a></dd>
                  <dd><a href="#panel2-2">Trending</a></dd>
                  <dd><a href="#panel2-3">Premium</a></dd>
                </dl>
                <div class="tabs-content">
                  <div class="content active" id="panel2-1">
                <!-- Home page featured posts wall -->

<?php if ( have_posts() ) : ?>
                      <?php while ( have_posts() ) : the_post(); ?>
        <div class="post-home">
            <?php 
                if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
                   echo "<div class=\"post-img\">";
                        the_post_thumbnail('thumbnail');
                   echo "</div>";
                } 
            ?>
            <h2 class="post-title"><a href="<?php echo get_permalink();?>"><?php the_title()?></a></h2>
            <p class="post-meta"><span class="post-exclusive">Exclusive</span>By <?php the_author_posts_link();?>, <?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></p>
            <p class="post-excerpt"><?php echo get_the_excerpt(); ?></p>
        </div>
                <hr>
                      <?php endwhile; ?>
                       <div class="navigation"><p><?php posts_nav_link('&#8734;','Newer Posts','Older Posts'); ?></p></div>
                    <?php 
            // Reset Query
                    wp_reset_postdata();
                    ?>
                     
                      <?php else:  ?>
  <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>
                        </div>
                    <div class="content" id="panel2-2">
                       <?php $query1 = new WP_Query( array ( 'orderby' => 'comment_count', 'order' => 'DESC' ) ); 
                       while ( $query1->have_posts() ) {
	$query1->the_post();
	echo '<li>' . get_the_title() . '</li>';
}     
                        wp_reset_postdata();
                        ?>
                    </div>
                    <div class="content" id="panel2-3">
                         <?php $query2 = new WP_Query( 'tag=premium') ; 
                       while ( $query2->have_posts() ) {
	$query2->the_post();
	echo '<li>' . get_the_title() . '</li>';
}     
                        wp_reset_postdata();
                        ?>
                    </div>
                    </div>
                </div>
                <?php get_sidebar(); ?>
        </div>
    </div>
</div>
    
<?php get_footer(); ?>